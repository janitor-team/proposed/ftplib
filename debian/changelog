ftplib (4.0-1-5) UNRELEASED; urgency=medium

  * d/changelog: Remove trailing whitespaces

 -- Ondřej Nový <onovy@debian.org>  Mon, 01 Oct 2018 10:24:46 +0200

ftplib (4.0-1-4) unstable; urgency=medium

  * Fix FTCBFS: Let dh_auto_build pass cross tools to make. (Closes: #901224)
    Thanks to Helmut Grohne for the patch.
  * Bump Standards-Version to 4.1.4.

 -- Raphaël Hertzog <hertzog@debian.org>  Thu, 28 Jun 2018 23:21:01 +0200

ftplib (4.0-1-3) unstable; urgency=medium

  * Switch Vcs-* fields to salsa.debian.org.
  * Switch to debhelper compat level 11.
  * Bump Standards-Version to 4.1.3.
  * Include dpkg's architecture.mk to get DEB_HOST_MULTIARCH as requested
    by lintian.
  * Add another missing LDFLAGS in upstream Makefile (spotted by blhc).
  * Update watch file to use https URL and fix it at the same time.

 -- Raphaël Hertzog <hertzog@debian.org>  Tue, 06 Feb 2018 11:23:46 +0100

ftplib (4.0-1-2) unstable; urgency=medium

  * Upload to unstable.
  * Add a working watch file.

 -- Raphaël Hertzog <hertzog@debian.org>  Thu, 21 Jul 2016 12:44:32 +0200

ftplib (4.0-1-1) experimental; urgency=low

  * New upstream release.
    - Dropped debian/patches/check-getservbyname-failure
    - Refreshed debian/patches/fix-ascii-read-without-eol
    - All perror calls are now protected with "if (ftplib_debug)".
      Closes: #24429
    - License changed to Artistic-2.0.
  * Rename ftplib3 into libftp4 for SONAME bump.
  * Rename ftplib-dev into libftp-dev (with "Provides: ftplib-dev" to not break
    existing packages, and Replaces for smooth upgrade).
  * Drop debian-generated manual pages as debian/html2man.py no longer copes
    with the updated documentation. Keep only the HTML documentation.
    This also helps to make the build reproducible. Closes: #831645
  * Use ${misc:Pre-Depends} instead of hardcoding multiarch-support.
  * Update Vcs-* fields to use https.
  * Update symbols file:
    - DefaultNetbuf has been dropped
    - FtpSetCallback/FtpClearCallback have been added
    - net_read/net_write have been added (although they are
      likely private symbols)
  * Switch debhelper compat to 9.
  * Patch upstream Makefile to pass CFLAGS/CPPFLAGS/LDFLAGS (for proper
    hardening support).
  * Bump Standards-Version to 3.9.8.

 -- Raphaël Hertzog <hertzog@debian.org>  Thu, 21 Jul 2016 12:17:12 +0200

ftplib (3.1-1-9) unstable; urgency=low

  * Add new patch debian/patches/fix-ascii-read-without-eol to fix a bug where
    FtpRead in ASCII mode would overwrite the first byte of data read with a
    NUL char if no EOL is found in the next block of data read.
  * Install library in multiarch path.
  * Set CFLAGS/LDFLAGS/CPPFLAGS via dpkg-buildflags.
  * Bump Standards-Version to 3.9.2.

 -- Raphaël Hertzog <hertzog@debian.org>  Sat, 11 Jun 2011 16:07:39 +0200

ftplib (3.1-1-8) unstable; urgency=low

  * Convert to 3.0 (quilt) source format. Let's try it out for real and see
    how buildd copes with it.
  * Move an (unnoticed) Debian specific change in a new quilt patch with its
    DEP-3 compliant header.

 -- Raphaël Hertzog <hertzog@debian.org>  Thu, 29 Oct 2009 17:07:46 +0100

ftplib (3.1-1-7) unstable; urgency=low

  * New maintainer. Closes: #353128
  * Fix the doc-base registration to mention section Programming and not
    Apps/Programming.
  * Fix RFC path given in README.Debian of ftplib-dev.
  * Add a symbols file for ftplib3.
  * Fix debian/html2man.py to escape dashes as \-.
  * Add a watch file indicating that upstream is not reliable in providing a
    consistent scheme for new releases (if any new release can be expected!).
  * Add Vcs-Git and Vcs-Browser URLs in debian/control.
  * Hardcode the dependency ftplib-dev → ftplib3 in debian/control
    instead of using debian/shlibs.local.
  * Drop Conflicts/Replaces fields introduced for a package split in 1998.
  * Update copyright file and add Homepage field.
  * Update Standards-Version to 3.8.3 (no supplementary changes needed).
  * Switch to debhelper 7 and tiny rules file.
  * Add a lintian override for package-name-doesnt-match-sonames as I don't
    plan to do a useless transition for now.

 -- Raphaël Hertzog <hertzog@debian.org>  Sun, 04 Oct 2009 19:07:28 +0200

ftplib (3.1-1-6) unstable; urgency=low

  * QA upload.
  * Package is orphaned (#353128); set maintainer to Debian QA Group.
  * Switch to debhelper.
  * Move ftplib-dev to libdevel in accordance with the override file.
  * Move qftp to /usr/lib/ftplib-dev/examples.
  * Move NOTES.qftp to /usr/share/doc/ftplib-dev/examples.
  * Replace /usr/share/doc/ftplib-dev/examples/README.qftp with a symlink
    to ../README.qftp.
  * Register documentation through doc-base rather than menu.
  * debian/Makefile.examples: Add symlinks for ftprm and ftplist.
  * debian/html2man.py: Create destination directory if necessary.
  * debian/changelog: Remove obsolete Emacs local variables.
  * debian/copyright: Update FSF address.
  * Conforms to Standards version 3.6.2.

 -- Matej Vela <vela@debian.org>  Thu,  9 Mar 2006 01:08:29 +0100

ftplib (3.1-1-5) unstable; urgency=low

  * Install NOTES file in ftplib-dev's documentation directory as
    NOTES.qftp
  * Don't compress very small documentation files (README.Debian, TODO).
  * Standards-Version 3.5.8.
  * Update package homepage and author's email address in debian/control.
  * debug -> noopt change: Build with debug support by default again.
  * Drop handling of /usr/doc symlink.
  * debian/rules: Get rid of double dependency of ftplib-dev on the
    runtime library, by munging the shlibdeps output for qftp.
  * debian/rules: Strip harder.

 -- Richard Braakman <dark@xs4all.nl>  Thu, 12 Dec 2002 05:30:33 +0200

ftplib (3.1-1-4) unstable; urgency=low

  * Change build dependency again from python-base (>= 1.5.2-4) to
    just python, because python-base no longer exists.  The package
    will still build on both potato and woody, because python-base
    in potato Provides: python.
    Thanks to Gregor Hoffleit for reporting the problem, and to
    David Kimdon for prodding me with a non-maintainer upload.
    (closes: bug#126079, doesn't build from source).

 -- Richard Braakman <dark@xs4all.nl>  Sat,  2 Feb 2002 00:47:42 +0200

ftplib (3.1-1-3) unstable; urgency=low

  * Change build dependency from python-net (>= 1.5) to
    python-base (>= 1.5.2-4), because python-net got merged in long ago.
    The package will still build on both potato and woody.
    Thanks to Ryan Murray for pointing this out.
    (closes: bug#100181, invalid build-depends).

 -- Richard Braakman <dark@xs4all.nl>  Sun, 10 Jun 2001 01:53:38 +0300

ftplib (3.1-1-2) unstable; urgency=low

  * Fixes bug spotted by Steve Fosdick.  ftplib could segfault if
    an unknown port was specified by name.

 -- Richard Braakman <dark@xs4all.nl>  Fri,  9 Feb 2001 12:13:11 +0200

ftplib (3.1-1-1) unstable; urgency=low

  * New upstream bugfixing patch.  It was available only as a patch.
    I applied it to the ftplib 3.1 sources and used that
    as the "upstream source" for this package.
  * (debian/copyright) Noted new location of upstream source.

 -- Richard Braakman <dark@xs4all.nl>  Fri,  2 Feb 2001 18:35:48 +0200

ftplib (3.1-2) unstable; urgency=low

  * Moved manpages and documentation to /usr/share.
  * Made debian/rules sensitive to DEB_BUILD_OPTIONS environment variable.
  * Handle /usr/doc compatibility symlinks in postinst and prerm.
  * Added Build-Depends.
  * Set Standards-Version to 3.2.1.

 -- Richard Braakman <dark@xs4all.nl>  Tue, 30 Jan 2001 20:07:30 +0200

ftplib (3.1-1) unstable; urgency=low

  * New upstream source.
  * (debian/rules) Use linux/Makefile again, since it includes my rules
    for making shared libraries.
  * (debian/Makefile.linux) Discarded.
  * (debian/shlibs.local) Updated minimum version to 3.1-1, because
    new functions were added to the lib.
  * (debian/rules) Install the symlinks recommended by README.qftp in
    /usr/doc/ftplib-dev/examples/, so that the example works right away.

 -- Richard Braakman <dark@xs4all.nl>  Tue,  7 Jul 1998 19:19:56 +0200

ftplib (3-4) unstable; urgency=low

  * (debian/NOTES) Noted source-dependency on python-net, for the benefit
    of people who want to recompile this package.

 -- Richard Braakman <dark@xs4all.nl>  Sun, 26 Apr 1998 20:56:12 +0200

ftplib (3-3) frozen unstable; urgency=low

  * Upgrade to Debian policy 2.4.1:
    Move ldconfig call from ftplib-dev postinst to ftplib3 postinst.
    Only call ldconfig if argument is "configure".

 -- Richard Braakman <dark@xs4all.nl>  Sun, 26 Apr 1998 20:56:12 +0200

ftplib (3-2) unstable; urgency=low

  * Put ftplib-dev in section devel, not libs.

 -- Richard Braakman <dark@xs4all.nl>  Sun, 15 Feb 1998 04:06:32 +0100

ftplib (3-1) unstable; urgency=low

  * New maintainer.
  * New upstream source.
  * Adjusted copyright file (ftplib is now under LGPL).
  * Changed shlibs.local to depend on ftplib3 (>= 3-1).
  * Choose soname of libftp.3 and library name libftp.3.0.
  * debian/rules: more thorough "clean" target.
  * Moved header file to /usr/include/ftplib.h rather than
    /usr/include/ftplib/ftplib.h
  * Install simpler Makefile.examples
  * Split into ftplib<so> and ftplib-dev packages.
  * Convert html docs to manpages and install those (as well as html versions).
  * Refer to rfc959 in the README.Debian instead of installing it.
  * Install menu entry for HTML documentation.
  * Include Section and Priority fields in binary packages.
  * Standards-Version 2.4.0.0.
  * Use -p when installing documentation files, to preserve timestamps.
  * Set timestamp of man pages equal to the timestamp of the html
    pages from which they were generated.

 -- Richard Braakman <dark@xs4all.nl>  Sun,  4 Jan 1998 16:43:15 +0100

ftplib (2-3) unstable; urgency=low

  * Cosmetic change (Fix bug #9658).
  * Orphaned the package.

 -- Vincent Renardias <vincent@waw.com>  Fri, 9 May 1997 14:41:58 +0200

ftplib (2-2) unstable; urgency=low

  * Rebuilt with libc6.

 -- Vincent Renardias <vincent@waw.com>  Fri, 2 May 1997 01:36:09 +0200

ftplib (1-5) unstable; urgency=low

  * Compiled with '-D_REENTRANT'.

 -- Vincent Renardias <vincent@waw.com>  Wed, 12 Feb 1997 00:57:59 +0100

ftplib (1-4) unstable; urgency=low

  * Moved into the "libs" section.
  * Provided a Makefile to build the examples.

 -- Vincent Renardias <vincent@waw.com>  Sat, 11 Jan 1997 20:20:48 +0100

ftplib (1-3) unstable; urgency=low

  * Added missing ftplib.shlibs file.

 -- Vincent Renardias <vincent@waw.com>  Sun, 1 Dec 1996 04:43:48 +0100

ftplib (1-2) unstable; urgency=low

  * Added a dynamic library version

 -- Vincent Renardias <vincent@waw.com>  Sat, 2 Nov 1996 00:15:23 +0100

ftplib (1-1) unstable; urgency=low

  * Initial Release

 -- Vincent Renardias <vincent@waw.com>  Sun, 20 Oct 1996 23:13:48 +0200
